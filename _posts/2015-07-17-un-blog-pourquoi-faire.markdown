---
layout: post
title:  "Un blog ? Pourquoi faire ?"
date:   2015-07-17 17:54:00
categories: meta
---

Par où commencer ? Si vous ne me connaissez pas (ou peu), je vous propose de faire un tour sur la [page "À propos"]({{ '/about' | prepend: site.url }}).

Donc voilà, j'ouvre un blog. Pourquoi ? Comme ça, j'ai envie. Qu'est-ce qu'il contiendra ? J'ai ma petite idée pour les deux ou trois premiers articles, mais en général ça parlera de tout et de rien. Majoritairement de mes centres d'interêt, à savoir la programmation. De toute manière, je ne compte pas simplement relayer les informations qui me paraissent intéressante. Pour cela, il y a mon compte Twitter, 140 caractères sont largement suffisants pour ce genre de choses. J'ai plus l'intention de créer du contenu original, ou semi-original, surtout si ce que je dit n'a pas ou peu été dit.

Je n'ai aucune envie de me forcer à mettre ce blog continuellement à jour, j'y posterais quand j'en aurais envie et parce que j'en ai eu l'envie. Donc un truc est sûr, je ne vous spammerais pas de posts. Vous pouvez dès maintenant vous inscrire à mon flux RSS, et l'oublier jusqu'à ce qu'à un post arrive ;-).

Je ne garantis pas non plus la véritabilité de ce que je dit, et vous suivez mes conseils à vos risque et perils. Ça m'arrivera sûrement de dire des bêtises, ou de faire des fautes, ou de vous dire que les touches <kbd>Alt</kbd>+<kbd>F4</kbd> vous configure votre système automatiquement ; je n'ai pas envie de m'embêter avec ça.

Voilà ! On peut résumer tout simplement :

**TL;DR** Ceci est un blog écrit par un feignant qui a juste envie de l'ouvrir :-)

