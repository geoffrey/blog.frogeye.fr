---
layout: post
title:  "Un problème tout con"
date:   2019-08-10 15:00:00
categories: linux grub arch probleme
---

Tout à l'heure,
en voulant paramètrer un add-on Firefox,
je me suis retrouvé avec un PC qui ne bootait plus ([relevant xkcd](https://www.xkcd.com/349/)).
Je vous passe les détails de comment je suis arrivé là,
mais comment j'ai **essayé** de le résoudre c'était plutôt intéressant.


En fait,
pour ma configuration BTRFS + dm-crypt,
j'ai besoin de trois options dans GRUB: `cryptdevice` pour lui dire qu'il doit déchiffrer tel disque,
`root` pour lui dire sur quoi il doit booter (le disque déchiffré) et `rootflags` pour lui dire de booter sur un certain sous-volume BTRFS.

Ces trois paramètres sont définis dans ma config GRUB,
sauf que depuis une mise à jour `root` et `rootflags` se génèrent automatiquement,
faisant en sorte que ces deux machins soient dupliqués,
ce que GRUB aime pas du tout,
d'où le système non-bootable [^passur].

[^passur]:
    J'en suis pas totalement sûr,
    peut-être qu'un truc que j'ai fais durant ce debug a corrigé le problème sans que je m'en rende compte.


Pour corriger le problème,
c'est très simple,
il suffit d'enlever les paramètres en double du fichier de config,
et si le système est déjà plus bootable,
le faire dans l'éditeur de config integré de GRUB. Hyper simple.

Sauf que ça,
je l'ai appris bien plus tard,
et entre temps j'ai énormément perdu de temps.
Voyez plutôt :

1. Forcer le reboot après avoir lancé des programmes qui abusaient un peu sur l'I/O.
1. Voir que ça démarre pas. Soupçonner la corruption disque à cause du reboot forcé.
1. [Tooter sur le fait](https://social.frogeye.fr/notice/9lm0GRCCNcS0ryZq0e).
1. Survoler la config GRUB délibérément ignorer le fait qu'il y a deux `root`: « Bah, ça devrait pas gêner c'est deux fois la même chose » (spoiler: non).
1. Préparer à manger en guise de procrastination en croyant connaître les étapes qui arrivent.
1. Télécharger la dernière ISO d'Arch via [DriveDroid](https://www.drivedroid.io/)[^drivedroid] sur mon téléphone, et attendre (oui ça aurait été moins con une étape plus tôt).
1. Tenter de booter dessus via DriveDroid, voir que ça marche pas.
1. Trifouiller les paramètres de DriveDroid.
1. Trifouiller les paramètres de boot du PC.
1. Lancer l'assistant de configuration de DriveDroid au cas où.
1. Abandonner et aller chercher une clef USB et un adaptateur USB-OTG pour graver l'image dessus.
1. Retrouver la bonne commande pour passer en root sur Termux.
1. Vérifier si il n'y a pas d'arguments nécessaire pour graver l'image avec `dd` sur le wiki Arch.
1. Se perdre sur le Arch Wiki à l'idée d'avoir une clef bootable Arch avec tous les drivers dont j'ai besoin dessus afin d'éviter 14 étapes.
1. [Tooter sur la reflexion](https://social.frogeye.fr/notice/9lm4PF3XHg5xdqyk2C).
1. Lancer `dd`, et attendre (oui ça aurait été moins con deux étapes plus tôt).
1. Booter sur la clef, voir que ça marche pas.
1. Défaire les paramètres trifouillés en 9.
1. Booter sur la clef, voir que ça marche.
1. Entrer en mode : « Va falloir installer des trucs, il me faut une connexion internet ».
1. Vouloir installer les pilotes Wi-Fi. Je vous passe les détails, c'est chiant.
1. Se rendre compte que c'était pas nécessaire parce qu'ils sont déjà inclus pour ce PC là.
1. Vouloir configurer `wpa_supplicant` en récupérant tant bien que mal de la conf un peu partout.
1. Se rendre compte que j'ai foutu ma conf `/etc` sur un git privé et que j'ai qu'à recopier la config.
1. Probablement quelques instants avant, se rendre compte qu'on utilie pas DriveDroid et qu'on peut faire un partage de connexion par le téléphone, c'est quand même beaucoup plus simple.
1. Faire un `pacman -Syu` parce que va falloir installer des trucs.
1. Essayer d'installer `yay` parce qu'on va avoir besoin de trus de AUR.
1. Raté, il faut un compte non-root pour créer des paquets. En créer un.
1. Lancer la création, partir manger (ça a refroidi en attendant) le temps que `go` se télécharge (oui j'ai pas été con cette fois. Sauf que...).
1. Revenir, voir que ça a foiré au bout de 60 secondes parce que `go` est super lourd et que le disque d'installation a un espace limité d'écriture (sur la RAM).
1. Taper la commande pour augmenter l'espace en écriture, voir que ça marche pas.
1. Chercher sur internet la bonne commande, pas comprendre parce que c'est ce qu'on a tapé.
1. Taper la commande pour augmenter l'espace en écriture SANS FAUTE DE FRAPPE, voir que ça marche.
1. Attendre que `go` se re-télécharge.
1. Juste quand c'est fini, se rendre compte que ça fait deux mois qu'on utilise plus ZFS mais BTRFS donc que les drivers sont déjà installés.
1. Tant qu'on y est, ptet que `dm-crypt` est déjà installé. Semblerait que oui. Oh cool !
1. Tester si `bcache` serait pas déjà là lui aussi. Non, `modprobe bcache` renvoie une erreur.
1. Se rendre compte qu'on a qu'un seul paquet à installer depuis AUR, `bcache-tools`. Annuler l'installation de `yay` et installation manuelle de `bcache-tools`.
1. Pardon, installation manuelle SANS FAUTE DE FRAPPE (oui ça m'a pris une putain de minute pour m'en rendre compte).
1. `modprobe bcache`. Module pas trouvé. What?.
1. Se rendre compte qu'en ayant fait un `pacman -Syu` à l'étape 26., on a installé Linux et ses headers version `5.2.8` alors que le LiveUSB a démarré en `5.2.5`. Du coup on a compilé bcache pour une version qu'on lance pas.
1. Essayer de forcer le chargement du module même si c'est pas la bonne version. Le système remarque le subterfuge, et refuse. Tsss...
1. Vouloir redémarrer. Taper `reboo` et se rendre compte que vu que c'est un LiveUSB, on perdra tous les changements et on redémarrera en `5.2.5`.
1. Essayer de trouver un moyen de faire persister les changements sur la clef.
1. Abandonner au bout de 5 minutes au regard de la complexité du truc.
1. Redémarrer le LiveUSB en pensant au moyen d'installer tout ce qui est nécessaire pour compiler `bcache-tools` sans mettre à jour Linux.
1. Taper `modprobe bcache` pour le voir pas trouver le module.
1. Ça marche. QUOI !? MAIS D'OÙ ???? DSAJ3R3QIJAKLDLKAJDKLSANLKDA .
1. Se rendre compte que `bcache-tools` inclus pas le module. Il est déjà inclus dans le LiveUSB, c'est juste en faisant un `pacman -Syu` j'ai remplacé le module du `5.2.5` par celui du `5.2.8`, incompatbile.
1. Voir que c'est marqué dans [le premier paragraphe sur bcache du wiki Arch](https://wiki.archlinux.org/index.php/Bcache), que j'avais ouvert sur mon téléphone depuis longtemps.
1. Aller faire un tour 10 minutes dehors, profiter du soleil pour se reconcentrer après.
1. Suivre minutieusement les étapes de [/dev/bcache device does not exist on bootup](https://wiki.archlinux.org/index.php/Bcache#/dev/bcache_device_does_not_exist_on_bootup).
1. Se planter quand même.
1. Réussir à déchiffrer le disque, le monter.
1. Chrooter dessus.
1. Pardon, chrooter sur le bon sous-volume.
1. Tenter un `pacman -Syu` (mer il é fou).
1. Foutre un dahua pas possible parce que j'ai pas monter les sous-volumes des sous-volumes. Les monter.
1. Re-tenter le `pacman -Syu`. Voir qu'il me demande d'accepter de nouvelles clefs PGP, les accepter.
1. Y a beaucoup de clefs PGP à accepter. Bon, admettons, ça vient ptet du dahua.
1. Attendre qu'il télécharge les paquets.
1. Y a un problème avec les clefs PGP, truc corrompus ou je sais pas quoi. Supprimer ? Oui.
1. Supprimer ? Oui. Supprimer ? Oui. Supprimer ? Oui. Supprimer ? Oui. Supprimer ? Oui.
1. Relancer la commande. Se rendre compte que ce que j'ai supprimé tous les paquets que je venais de télécharger.
1. Attendre que ça se re-télécharge.
1. Pester pourquoi j'ai pas fait un `pacman -Syu` plus récemment.
1. Supprimer ? NON VA TE FAIRE FOUTRE.
1. Trifouiller avec `pacman-key` pour récupérer les clefs GPG. Erreur. Erreur.
1. Supprimer TOUTES les clefs PGP, relancer un `pacman-key --init` et un `pacman-key --populate`. Aucune erreur.
1. Relancer `pacman -Syu`. « J'ai pas de clef PGP ! ». Ah, je suis ptet allé un peu loin.
1. Réinstaller `archlinux-keyring` comme un sale à coup de `tar xzvf`.
1. Relancer un `pacman-key --init` et un `pacman-key --populate`. Erreur. Erreur. ERREUR.
1. Chercher sur internet les messages d'erreur. Tomber sur deux bugs de 2014 et 2017 avec les mêmes symptômes mais marqué comme corrigé depuis.
1. Ça me casse les coilles de chercher comment corriger ce problème sur mon téléphone, du coup je désactive temporairment la vérification de clefs PGP pour pacman et on verra plus tard.
1. `pacman -Syu`. « Pas trouvé / ». « Pas assez d'espace disque ». « Refuse d'installer ».
1. Ah oui, avec le `chroot`, `/` est pas réellement monté. Le monter.
1. `pacman -Syu`. « Pas pu trouvé / dans /etc/mtab ». « Pas assez d'espace disque ». « Refuse d'installer ».
1. Mais `/etc/mtab` c'est celui du système hôte, pas celui du chroot. Rooh, et puis va te faire foutre, je re-nique mon `/etc/pacman.conf` pour passer la vérification d'espace libre.
1. `pacman -Syu`. J'appréhende la prochaine emmerde. Roulements de tambours... Oh ça a marché. Ben ça alors.
1. Avec tous les hooks qu'il a lancé, je me dit qu'il a ptet corrigé le problème, un petit reboot pour tester ça peut pas faire de mal.
1. Avant ça, je re-regarde quand même `/boot/grub/grub.cfg` pour voir si tout va bien, tu sais, la même chose que l'étape 4., juste pas depuis Grub directement.
1. ENFIN, se redre compte du vrai problème. Corriger `/etc/default/grub`, et lancer `grub-mkconfig`. Rebooter.
1. Ça reboote pas. Évidemment, j'ai lancé un chroot comme un pécore du coup moon `/boot/grub/grub.cfg` utilise des préfixes du genre `/boot/archlinux/` au liu de `/`...
1. Changer les 2 occurences du préfixe dans l'éditeur de config de GRUB, rebooter.
1. Pardon, changer les 3 préfixes. Rebooter.
1. Pardon, changer les 4 préfixes. Rebooter.
1. Welcome to ~~[Mario Kart](https://youtu.be/Qxn3g9qbF3Q?t=9)~~ Arch Linux!
1. Voir que tout fonctionne à partir de là. Respirer.

[^drivedroid]:
    C'est une application qui permet de faire croire à votre PC que votre téléphone est une clef USB.
    Vous pouvez vous en servir (entre autres) pour installer une distro avec son image d'installation,
    ça vous évite de la graver.


Putain.
Tout ça pour ça.

Morale de l'histoire ?
Vous avez beau connaître votre domaine sur le bout des doigts,
vous vous passerez toujours trois heures sur des trucs à la con.

J'ai ai dépanné des systèmes qui bootaient pas.
J'en ai fait des chroots.
J'ai ai parametré des systèmes avec des configurations alambiquées pour pouvoir bien comprendre comment ça fonctionne.
Mais ça m'a pas enpêché de me foutre des bâtons dans les roues à une étape sur deux de la résolution de ce problème :D.

Petit message à tous les nouveaux administrateurs systèmes en herbe.
Si vous passez trois heure sur un truc tout con,
c'est normal,
Ça arrive même aux meilleurs [^no_ego].
En fait, vous avez même l'avantage de bien analyser le problème et de ne pas sauter sur des conclusions hâtives qui vous feront perdre du temps.
Courage :)

[^no_ego]:
    Je parle pas de moi là, hein :D

Bon, sur ce je vous laisse, je dois

1. Corriger mon `grub.cfg`
1. Rectifier le bordel que j'ai mis dans mon `pacman.conf`
1. Corriger le bordel des clefs de pacman
1. Tooter pour se marrer du fait que je suis allé loin pour un souci de ce genre
1. Écrire le toot sur un fichier parce qu'il commence à devenir long et si le navigateur crashe j'aimerais pas le réécrire
1. Tant qu'à faire, autant publier un article de blog
1. Oh wait
