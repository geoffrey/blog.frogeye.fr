---
layout: post
title:  "Accéder à un périphérique bloc à distance"
date:   2018-10-21 14:00:00
categories: linux rescue disk network
---

<!-- L'article a été rédigé le 2018-10-22 mais Jekyll n'aime pas cette date, allez savoir -->

# Contexte
J'aime beaucoup tester des trucs sur mon serveur, notamment avec le système de fichiers BTRFS.
Sauf que j'ai fait l'erreur de [compresser `/boot` avec une méthode que Grub ne supporte pas](https://askubuntu.com/q/1056396/342879), et après un [reboot malencontreux](https://oc.todon.fr/@GeoffreyFrogeye/100938592425275062) mon système ne peux plus booter.
Qu'à cela ne tienne, je me pointe vers mon panel OVH et démarre en rescue avec une image d'Arch.
Sauf que le mode rescue utilise la clef SSH que j'ai utilisé pour créer le serveur et que j'ai détruite depuis le temps, et [impossible de la changer](https://twitter.com/GeoffreyFrogeye/status/1054315898566250496).

Ma seule option est donc de démarrer sur l'image de rescue « Made in OVH », dont le noyau n'est pas suffisament récent pour monter ma partition BTRFS (à jouer avec le feu...).
Plutôt que de recompiler un noyaux linux avec les bonnes options, je préfère plutôt monter le disque à distance depuis mon laptop et réparer mes dégâts d'ici (ça implique une bonne connexion).

# Réalisation

> **Avertissement :** Je vous donne ma méthode quick'n'dirty à défaut d'avoir trouvé quelque chose qui expliquait ce que je voulais faire simplement, mais je vous renvoie à la documentation si vous souhaitez utiliser ça de manière propre.
> Je ne sais d'ailleurs aucunement si il y a une méthode de correction d'erreur, donc à vos risques et périls (et évitez le Wi-Fi).

Sur la machine avec le périphérique bloc à monter (dans mon cas `/dev/sdb1`), installez le paquet `nbd-server` (ou `nbd` selon la distribution).
Modifiez ensuite le fichier `/etc/nbd-server/config` comme suit:

```ini
[generic]
    # Mettez en root ou changez les permissions de l'utilisateur `nbd` pour
    # qu'il puisse ouvrir le périphérique voulu.
    user = root 
    group = root
[export1]
    exportname = /dev/sdb1
```

Ensuite, lancez `nbd-server 4567`, 4567 étant un numéro de port choisi aléatoirement.
À noter que le processus se fork en arrière plan donc pas de sortie et il vous rend l'accès au terminal tout de suite.
Pour vérifier que ça a fonctionné, `netcat localhost 4567` devrait vous afficher quelque chose commencant par `NBD`.

> **Attention :** En lançant cette commande vous exposez n'importe qui pouvant accéder à la machine peut lire/écrire dans votre périphérique.
> Personnellement j'ai joué la carte du « Qui va trouver et exploiter la vulnérabilité en 10 minutes sérieux ? » mais je vous renvoie au manuel de `nbd-server` pour ajouter au moins un filtrage IP.

Sur la machine qui va monter le périphérique, installez le paquet `nbd-client` (ou `nbd` selon la distribution).
Lancez ensuite `nbd-client -no-optgo 10.0.0.42 7894 -N export1` avec 10.0.0.42 l'ip de la machine avec le périphérique à monter.
L'option `-no-optgo` n'était nécessaire que dans mon cas, la version de `nbd-client` étant superieure à celle de `nbd-server`

Vous devriez obtenir un message vous informant que vous avez créé le périphérique `/dev/nbd0`, que vous pouvez désormais monter comme si vous étiez sur la machine distante.
Sur une connexion à 100 Mbps rien ne m'a semblé plus lent que si j'étais en local, mais je n'ai pas non plus testé énormément de choses.

# Références

- [Readme du projet NBD](https://github.com/NetworkBlockDevice/nbd/blob/master/README.md)
- Le manuel de NBD
- [La seule occurence de NBD sur le wiki Arch](https://wiki.archlinux.org/index.php/Diskless_system#NBD_2)

