---
layout: post
title:  "Configurer eduroam pour une connexion WPA-EAP TLS avec un certificat au format .p12"
date:   2018-10-23 13:00:00
categories: eduroam wifi wpa-supplicant
---

<!-- Again article écrit le 2018-10-24 mais jekyll being jekyll...
-->

Si vous étudiez / travaillez dans une université il y a de grandes chances pour que celle-ci propose une connexion via le réseau Wi-Fi eduroam.
Une fois connecté, à ce réseau avec les identifiants de votre université, vous pouvrez vous y connecter dans n'importe quelle université proposant aussi eduroam, ce qui est plutôt chouette.

Certaines universités vous proposent de vous connecter via un identifiant et un mot de passe d'autres vous proposent de vous connecter avec un certificat et une clef privée.
C'est plus complexe à mettre en place, mais c'est plus safe et si vous changez de mot de passe (ce qu'il faut faire régulièrement) ça reste configuré sur tous vos appareils.
Pour simplifier la configuration du système pour les utilisateurs, eduroam propose un outil appelé `eduroamCAT` qui configure la connexion pour vous.

Sauf que pour moi, que ce soit sur mon Arch avec `wpa_supplicant` ou sur mon téléphone Android, j'ai eu quelques difficultés à installer le certificat fourni par mon université, dans mon cas au format `.p12`.
Voici ce que j'ai fais pour y remédier, si d'autres personnes sont dans le même cas.

# Android

J'ai d'abord téléchargé sur le site de mon université le certificat avec l'extension `.p12` donné avec un mot de passe pour le déchiffrer.
En l'ouvrant simplement depuis mon gestionnaire de fichiers j'ai eu le droit à pleins de comportements indésirables de l'application censée l'installer : crashs, freezes, messages d'erreur...

C'est probablement lié à ma ROM, mais dans mon cas il faut simplement l'installer depuis les paramètres de sécurité du téléphone (Installer un certificat depuis la carte SD).
Tapez le mot de passe pour le déchiffrer, donnez-lui un nom bidon et c'est bon.

Tant que vous êtes dans les paramètres, oubliez le réseau `eduroam` pour éviter les conflits.

Ensuite, je suis allé sur le site <https://cat.eduroam.org> pour télécharger le profil de mon université.
Encore une fois, ouvrir le fichier directement ça fonctionne pas, donc j'ai dû l'ouvrir depuis l'application.

Après c'est assez *buttersmooth*, entrez votre identifiant (du type `monidentifiant@monuniv.tld`) et la config se fait automatiquement, il vous faut juste retourner dans les paramètres Wi-Fi pour choisir le bon certificat utilisateur puisque l'application ne peut pas le faire elle-même.

Si votre université propose un deuxième réseau Wi-Fi protegé aussi basé sur eduroam (dans mon cas `monuniv-protected`) il vous suffit de copier/coller les paramètres depuis le réseau `eduroam` puisque l'application ne le fait pas pour vous.

# wpa_supplicant

Encore une fois téléchargez sur le site de votre université le certificat avec l'extension `.p12` donné avec un mot de passe pour le déchiffrer.

Vous pouvez aller sur <https://cat.eduroam.org> pour télécharger un script Python avec la configuration integrée mais le script crashe si Network Manager n'est pas installé donc il faut le forcer à faire une config `wpa_supplicant`.

Voilà le messsage d'erreur si NetworkManager n'est pas installé (oui c'est principalement pour le référencement):

```
$ python ./eduroam-linux-PdM-UNIV.py
eduroam-linux-PdM-UNIV.py:149: DeprecationWarning: dist() and linux_distribution() functions are deprecated in Python 3.5
  system = platform.linux_distribution()
Traceback (most recent call last):
  File "/usr/lib/python3.7/site-packages/dbus/bus.py", line 175, in activate_name_owner
    return self.get_name_owner(bus_name)
  File "/usr/lib/python3.7/site-packages/dbus/bus.py", line 361, in get_name_owner
    's', (bus_name,), **keywords)
  File "/usr/lib/python3.7/site-packages/dbus/connection.py", line 651, in call_blocking
    message, timeout)
dbus.exceptions.DBusException: org.freedesktop.DBus.Error.NameHasNoOwner: Could not get owner of name 'org.freedesktop.NetworkManager': no such name

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "eduroam-linux-PdM-UNIV.py", line 995, in <module>
    run_installer()
  File "eduroam-linux-PdM-UNIV.py", line 197, in run_installer
    if config_tool.connect_to_nm() is None:
  File "eduroam-linux-PdM-UNIV.py", line 779, in connect_to_nm
    "/org/freedesktop/NetworkManagerSettings")
  File "/usr/lib/python3.7/site-packages/dbus/bus.py", line 241, in get_object
    follow_name_owner_changes=follow_name_owner_changes)
  File "/usr/lib/python3.7/site-packages/dbus/proxies.py", line 248, in __init__
    self._named_service = conn.activate_name_owner(bus_name)
  File "/usr/lib/python3.7/site-packages/dbus/bus.py", line 180, in activate_name_owner
    self.start_service_by_name(bus_name)
  File "/usr/lib/python3.7/site-packages/dbus/bus.py", line 278, in start_service_by_name
    'su', (bus_name, flags)))
  File "/usr/lib/python3.7/site-packages/dbus/connection.py", line 651, in call_blocking
    message, timeout)
dbus.exceptions.DBusException: org.freedesktop.DBus.Error.ServiceUnknown: The name org.freedesktop.NetworkManager was not provided by any .service files
```

Du coup voilà la modif à faire:

```diff
diff --git a/eduroam-linux-PdM-UNIV.py b/eduroam-linux-PdM-UNIV.corrected.py
index 576126b..3ad59f0 100644
--- a/eduroam-linux-PdM-UNIV.py
+++ b/eduroam-linux-PdM-UNIV.corrected.py
@@ -194,7 +194,7 @@ def run_installer():
     # test dbus connection
     if NM_AVAILABLE:
         config_tool = CatNMConfigTool()
-        if config_tool.connect_to_nm() is None:
+        if None is None:
             NM_AVAILABLE = False
     if not NM_AVAILABLE:
         # no dbus so ask if the user will want wpa_supplicant config
```

Lancez-le (`python eduroam-linux-PdM-UNIV.py`), entrez (dans le désordre) votre identifiant (du type `monidentifiant@monuniv.tld`), le chemin du certificat et son mot de passe de déchiffrement.

Une fois terminé, vous allez avoir des fichiers dans `~/.cat_installer`.
`cat_installer.conf` contient la configuration de votre réseau Wi-Fi que vous pouvez rajouter à votre fichier `/etc/wpa_supplicant/wpa_supplicant.conf` (ou autre, selon votre configuration).
Cependant il va falloir remplacer un bon nombre de choses :

- L'indentation est pétée
- Il faut mettre le SSID entre guillemets
- Il faut indiquer le chemin du certificat et la clef privée de l'utilisateur (et non, ils ne sont pas mis par le script), par exemple `client_cert="/home/geoffrey/.cat_installer/cert.pem"` et `private_key="/home/geoffrey/.cat_installer/key.pem"`.
- `password=` n'a rien à faire dans une connexion par certificat, il vous faut mettre `private_key_passwd=` à la place (vous pouvez utiliser un autre mot de passe, à partir de l'étape suivante on pourra se débarasser du mot de passe donné à la prochaine étape)
- Dans mon cas `pairwise=CCMP` était pas suffiant, j'ai dû utiliser `pairwise=CCMP TKIP`
- Dans mon cas j'ai dû corriger `phase2=` avec `phase2="auth="`

Il faut désormais créer les fichiers `.pem` referencés précédemment, à partir du fichier `.p12`.
Un fichier `user.p12` a été créé dans `~/.cat_installer` mais il n'avait pas le même hash que fichier `.p12` téléchargé donc j'ai préféré ne pas l'utiliser.
Du coup :

```bash
openssl pkcs12 -in fichier.p12 -nokeys -out cert.pem
openssl pkcs12 -in fichier.p12 -nocerts -out key.pem
```

Pour les deux commandes, on vous demandera dans un premier temps le mot de passe fourni avec le fichier `.p12` et pour la deuxième commande on vous demandera une clef PEM, utilisez ce que vous avez mis dans `private_key_passwd=` dans la conf.

Il vous faudra peut-être enlever ce qui n'est pas dans les `-----BEGIN ...-----` → `-----END ...-----` dans les fichiers générés.<!-- Flemme de vérifier si c'est vraiment utile -->

Voilà, redémarrez `wpa_supplicant` et ça devrait fonctionner.
