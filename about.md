---
layout: page
title: À propos
permalink: /about/
---

> Mais qui c'est celui-là, ce Gauffrette Œil-de-grenouille ?

Hé bien je me présente, je m'appelle Geoffrey Preud'homme, aussi connu sous le nom de Geoffrey Frogeye. Je suis un étudiant de 18 ans en prépa intégrée à l'école [Polytech Lille](http://www.polytech-lille.fr).

Mon hobby c'est la programmation, et je fais un peu de tout : NodeJS, Python, PHP, Bash, C++... Vous pouvez retrouver une partie de ce que je code sur [mon profil GitHub](http://github.com/GeoffreyFrogeye).

Je soutiens le logiciel libre, et en utilise quasi-exclusivement. C'est pourquoi quand j'en ai la possibilité ce que je programme est aussi libre.

Toujours rasé <!-- ou presque -->, mais barbu dans l'âme, j'utilise le plus souvent des programmes très efficaces et qui regorgent de config,  mais pas forcément intuitif ni jolis. Je parle entre autre d'Arch Linux, de Vim, d'i3, ffmpeg et j'en passe.

Pour être sûr qu'aucun organisme ne fouine dans mes données privées, tous mes services -de la synchronisation de fichiers jusqu'au stockage de mes clefs PGP- sont hébergés de manière artisanale. Du coup, je touche un peu à tout en configuration de serveurs. 

Voilà, vous savez à peu près tout ce qu'il y a d'interessant.
