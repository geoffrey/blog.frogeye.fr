---
layout: post
title:  "Unbound, NTP et pas d'horloge"
date:   2016-06-20 13:41:00
categories: unbound ntpd hwclock
---

Si comme moi vous faites tourner un serveur DNS Unbound et un serveur NTP sur une machine sans horloge materielle tel qu'un Raspberry Pi, il est possible qu'après un redémarrage l'horloge soit à la mauvaise date et les connexion internet ne se font pas. On peut voir dans les logs d'Unbound la chose suivante :
```
failed to prime trust anchor -- DNSKEY rrset is not secure . DNSKEY IN
```

En fait, ce qu'il se passe est que vu que l'heure au démarrage n'est pas la bonne, Unbound n'accepte pas les certificats que lui envoient les serveur DNS racine sur lequel il se base, pour peu qu'il soit configuré pour vérifier l'authenticité des données qu'il reçoit (ce qui est recommandé). Du coup tous les logiciels qui essaient de résoudre un nom de domaine auprès d'Unbound se voient reçevoir un enregistrement vide, ce qui leur empêche de basculer automatiquement sur un serveur DNS de secours. Enfin, le serveur NTP ne peut pas résoudre les DNS des serveurs auquel il se connecte, et ne peut donc remettre l'heure correcte, et la situation ne peut se débloquer d'elle-même.

La façon la plus correcte de corriger ce problème définitivement est de désactiver la vérification 
